from setuptools import setup

setup(name='gromologist',
      version='0.300',
      description='Library to handle various GROMACS-related stuff',
      author='Milosz Wieczor',
      author_email='milafternoon@gmail.com',
      license='GNU GPLv3',
      packages=['gromologist'],
      zip_safe=False)
